En lien avec les PRBI, nous avons a extrait les données de statistiques Canada 2016 sur les revenus des ménages. Vous pouvez afficher les données en fonction de deux variables :

-   Les Population;

-   La région administrative

Cette application n'intègre pas les données des entités suivantes :

-   Les municipalités situées dans les limites d'une région métropolitaine;

-   Les municipalités de plus de 25 000 habitants;

-   Les territoires non organisés (TNO);

-   Les territoire où la Loi sur l'aménagement et l'urbanisme n'est pas appliquée.
